import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
