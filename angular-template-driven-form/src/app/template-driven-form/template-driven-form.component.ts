import { Component } from '@angular/core';
import { Task } from '../../Task'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.css']
})
export class TemplateDrivenFormComponent {
  tasks: Task[] = [];
  categories = ['School', 'Work', 'Hobby'];

  onSubmit(form: NgForm) {
    const { taskName, category } = form.value;
    console.log(taskName);
    console.log(category);
    this.tasks = [...this.tasks, new Task(taskName, false, category)];
    form.reset();
  }

}
