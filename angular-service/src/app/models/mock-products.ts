import { Product } from "./product";

export const PRODUCTS: Product[] = [
    {
        id: 'ABC123456',
        name: 'T-Shirt',
        description: 'Lorem ipsum dolor sit amet,'
    },
    {
        id: 'DEF4567889',
        name: 'Handbags',
        description: 'do nisi veniam occaecat veniam officia cillum'
    },
    {
        id: 'GHI321345',
        name: 'Shoes',
        description: 'sint enim nulla laboris proident quis aliquip et'
    }
]