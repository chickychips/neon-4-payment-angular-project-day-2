import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product';
import { PRODUCTS } from 'src/app/models/mock-products';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  getProducts(): Observable<Product[]> {
    const products = of (PRODUCTS);
    console.log("fetched list of products ...")
    return products;
  }
}
