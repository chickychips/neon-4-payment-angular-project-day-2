import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products = [
    {
      id: 'ABC123456',
      name: 'T-Shirt',
      description: 'Lorem ipsum dolor sit amet,'
    },
    {
      id: 'DEF456788',
      name: 'Shoes',
      description: 'do et nisi nulla aute irure veniam dolore pariatur'
    },
    {
      id: 'GHI987654',
      name: 'Handbags',
      description: 'qui commodo sint laboris aliqua non'
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
